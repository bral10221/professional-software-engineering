# Python

Download Python from <https://www.python.org/downloads/> and install it.

You can check for a successful installation with command

```
python --version
```

which should print the downloaded version.


## Pip

Pip is the package manager for installing Python dependencies. It is installed with Python by default.

To install a new package run

```
pip install <package>
```

and to update an existing package to the latest version run

```
pip install --upgrade <package>
```

## Virtual environment

Native Python installations only support one version of a package at a time. If a package is added to the native Python installation, all Python programs can use it. At first this might sound nice because it saves efforts but for developers it is a real nightmare. Why? Let's see.

A developer

-   wants to use different versions of packages for different tools
-   wants to install and test packages without breaking functionalities of other tools
-   wants to easily setup a developer environment for new projects
-   needs a constant development environment for collaboration (not "works on my machine" like)

To adress all of the points above, Python introduced sandboxes which are known as *virtual environments*. Especially on Linux the usage of virtual environments is essential because many operating system tools are implemented in Python (e.g. package manager apt). When installing or deleting Python dependencies it can result in a disfunctional operating system.

Technically, a virtual environment is a clone of a native Python installation which is copied to a custom folder (typically in folder `.venv` or `venv` in a project's root).

## Pipenv

Pip does not nativly support virtual environments. So there is a need of another tool that combines the power of pip and virtual envs. Pipenv is a feature rich tool that is developed for exactly that use case.

1. Install pipenv

   ```
   $ pip install pipenv
   ```

1. Check pipenv installation

   ```
   $ pipenv --version
   ```

1. Set environment variable `PIPENV_VENV_IN_PROJECT=1`

   **Windows**

   ```
   $ set PIPENV_VENV_IN_PROJECT=1
   ```

   **Linux**

   ```
   $ export PIPENV_VENV_IN_PROJECT=1
   ```

1. Create a new folder for the project

   ```
   $ mkdir -p $HOME/git/hs-karlsruhe/my-first-pipenv-project
   ```

1. Navigate to the folder

   ```
   $ cd $HOME/git/hs-karlsruhe/my-first-pipenv-project
   ```

1. Initialize pipenv project to the existing folder

   ```
   $ mkdir -p hs-karlsruhe/my-first-pipenv-project
   $ pipenv --python python3
   Creating a virtualenv for this project…
   Pipfile: /home/sm/git/lab/hs-karlsruhe/my-first-pipenv-project/Pipfile
   Using /usr/bin/python3.8 (3.8.2) to create virtualenv…
   ⠏ Creating virtual environment...created virtual environment CPython3.8.2.final.0-64 in 635ms
     creator CPython3Posix(dest=/home/sm/git/lab/hs-karlsruhe/my-first-pipenv-project/.venv, clear=False, global=False)
     seeder FromAppData(download=False, CacheControl=latest, pyparsing=latest, requests=latest, pytoml=latest, pkg_resources=latest, idna=latest, lockfile=latest, certifi=latest, pip=latest, distlib=latest, retrying=latest, urllib3=latest, pep517=latest, html5lib=latest, msgpack=latest, wheel=latest, appdirs=latest, progress=latest, packaging=latest, distro=latest, six=latest, setuptools=latest, contextlib2=latest, webencodings=latest, chardet=latest, colorama=latest, ipaddr=latest, via=copy, app_data_dir=/home/sm/.local/share/virtualenv/seed-app-data/v1.0.1.debian)
     activators BashActivator,CShellActivator,FishActivator,PowerShellActivator,PythonActivator,XonshActivator

   ✔ Successfully created virtual environment!
   Virtualenv location: /home/sm/git/lab/hs-karlsruhe/my-first-pipenv-project/.venv
   Creating a Pipfile for this project…
   ```

1. Check the files and folders of your project

   ```
   $ ls -a
   .  ..  Pipfile  .venv
   ```

1. Check the content of `Pipfile`

   ```
   $ cat Pipfile
   [[source]]
   name = "pypi"
   url = "https://pypi.org/simple"
   verify_ssl = true

   [dev-packages]

   [packages]

   [requires]
   python_version = "3.8"
   ```

1. Install pip package `pytest` and `pytest-cov`

   ```
   $ pipenv install --dev pytest pytest-cov
   Installing pytest…
   Adding pytest to Pipfile's [dev-packages]…
   ✔ Installation Succeeded
   Installing pytest-cov…
   Adding pytest-cov to Pipfile's [dev-packages]…
   ✔ Installation Succeeded
   Pipfile.lock not found, creating…
   Locking [dev-packages] dependencies…
   ✔ Success!
   Locking [packages] dependencies…
   Updated Pipfile.lock (3e3adb)!
   Installing dependencies from Pipfile.lock (3e3adb)…
     🐍   ▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉ 11/11 — 00:00:05
   To activate this project's virtualenv, run pipenv shell.
   Alternatively, run a command inside the virtualenv with pipenv run.
   ```
