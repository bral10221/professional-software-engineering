# professional-software-engineering

## Content

- [Git Basics](Git.md)
- [Git Cheatsheet](Git-cheatsheet.md)
- [Python.md](Python.md)
- [PyCharm.md](PyCharm.md)

## Literature

- Linux
  - <https://training.linuxfoundation.org/training/introduction-to-linux>
- Git
  - <https://git-scm.com/book>
- GitLab
  - <https://docs.gitlab.com/ee/ci>
- Python
  - <https://docs.python.org/3/tutorial>
  - <https://dev.seperians.es/libros/Clean%20Code%20In%20Python.pdf>
- Docker
  - <https://docs.python.org/3/tutorial>
- Kubernetes
  - <https://kubernetes.io>
- Helm
  - <https://helm.sh/docs>

## Installation

- Linux: <https://training.linuxfoundation.org/training/introduction-to-linux>
- Git: <https://git-scm.com/book/en/v2/Getting-Started-Installing-Git>
- Python: <https://www.python.org/downloads/>
- PyCharm: <https://www.jetbrains.com/pycharm/>
- Docker: <https://docs.docker.com/install>
- Kubectl: <https://kubernetes.io/docs/tasks/tools/install-kubectl/>
- Helm: <https://helm.sh/docs/intro/install/>

## Kubernetes Class Diagram

```plantuml
class Deployment {
  metadata : ObjectMeta
  replicas : Integer
  selector : Object
  spec : DeploymentSpec
  -status : DeploymentStatus
}

class DeploymentSpec {
  replicas : Integer
  selector : Object
  strategy : DeploymentStrategy
  template : PodTemplate
}

class Pod {
  metadata : ObjectMeta
  spec : PodSpec
  -status : PodStatus
}

class PodTemplate {
  metadata : ObjectMeta
  template : PodTemplateSpec
}

class PodTemplateSpec {
  metadata : ObjectMeta
  spec : PodSpec
}

class PodSpec {
  containers : List<Container>
  imagePullSecrets : List<SecretReference>
  initContainers : List<Container>
  securityContext : PodSecurityContext
  volumes : List<Volume>
}

class Container {
  command: List<String>
  env : EnvVar
  envFrom : List<EnvFromSource>
  image : String
  imagePullPolicy : String
  lifecycle : Lifecycle
  livenessProbe : Probe
  name: String
  ports : List<ContainerPort>
  readinessProbe : Probe
  resources : ResoureRequirements
  securityContext : SecurityContext
  volumeMounts : List<VolumeMounts>
}

class PodSecurityContext {
  runAsGroup : Integer
  runAsUser : Integer
}

class SecurityContext {
  runAsGroup : Integer
  runAsUser : Integer
}

class EnvFromSource {
  configMapRef : ConfigMapEnvSource
  secretRef : SecretEnvSource
}

class SealedSecret {
  metadata : ObjectMeta
  encryptedData : Object
  template : SecretTemplate
}

class SecretTemplate {
  type : String
  metadata : ObjectMeta
}

class Secret {
  metadata : ObjectMeta
  stringData : Object
  data : Object
  type : String
}

class ConfigMap {
  metadata : ObjectMeta
  data : Object
}

class Service {
  metadata: ObjectMeta
  spec : ServiceSpec
  -status : ServiceStatus
}

class ServiceSpec {
  clusterIP : String
  ports : List<ServicePort>
  selector : Object
  type : String
}

class IngressRoute {
  metadata : Object
  spec : IngressRouteSpec
}

class IngressRouteSpec {
  entryPoints : List<String>
  routes : List<Route>
  tls.secretName : String
}

class Route {
  match : String
  kind : String
  services : ServiceSelector
}

class NetworkPolicy {
  metadata : ObjectMeta
  egress : NetworkPolicyEgressRule
  ingress : NetworkPolicyIngressRule
  podSelector : LabelSelector
  policyTypes : List<Enum[Ingress, Egress]>
}

class Volume {
  emptyDir : EmptyDirVolumeSource
  configMap : ConfigMapVolumeSource
  name : String
  secret : SecretVolumeSource
}

class CronJob {
  metadata : ObjectMeta
  spec : CronJobSpec
  -status : CronJobStatus
}

class CronJobSpec {
  concurrencyPolicy : String
  jobTemplate : JobTemplateSpec
  schedule : String
}

class JobTemplateSpec {
  metadata : ObjectMeta
  spec : JobSpec
}

class JobSpec {
  parallelism : Integer
  selector : LabelSelector
  template : PodTemplateSpec
}

class Job {
  metadata : ObjectMeta
  spec : JobSpec
  -status : JobStatus
}

Deployment "1" *-- DeploymentSpec
DeploymentSpec "1" *-- PodTemplate

PodTemplate "1" *-- PodTemplateSpec
PodTemplateSpec "1" *-- PodSpec
PodTemplateSpec "0..n" <. NetworkPolicy : selector
PodTemplateSpec "0..n" <. Service : selector

PodSpec "0..n" *-- Volume
PodSpec "0..1" *-- PodSecurityContext
PodSpec "0..n" *-- Container : init
PodSpec "1..n" *-- Container : container
Container "0..1" *-- SecurityContext
Container "0..n" *-- EnvFromSource
EnvFromSource ..> "0..1" Secret : references
EnvFromSource ..> "0..1" ConfigMap : references

CronJob "1" *-- CronJobSpec
CronJobSpec "1" *-- JobTemplateSpec
JobTemplateSpec "1" *-- JobSpec

Job "1" *-- JobSpec
JobSpec "1" *-- PodTemplateSpec

Pod "1" *-- PodSpec

Service "1" *-- ServiceSpec

IngressRoute "1" *-- IngressRouteSpec
IngressRouteSpec "1..n" *-- Route
Service "0..n" <. IngressRoute : selector

Secret "1" <-- SealedSecret : generates
SealedSecret "1" *-- SecretTemplate

Volume ..> "0..1" Secret : references
Volume ..> "0..1" ConfigMap : references
```
